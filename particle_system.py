import sys
import math
import numpy as np
import moldyn as md
class ParticleSystem:

	def __init__(self, N, Lx, Ly, kIni, dt, start):
		self.Lx       = Lx
		self.Ly       = Ly
		self.dt       = dt
		self.kIni     = kIni
		self.N        = N
		md.md.init_system(N, Lx, Ly, dt)

		self.reset_obs()

		if start=='lattice':
			self.init_vectors()
			self.create_lattice()
		else:
			fileconf = start+".cnf"
			if not os.path.isfile(fileconf):
				sys.exit("\n\n  File %s does not exist\n\n" % fileconf)
			indata = np.loadtxt(fileconf)
			# In this first version N is completely ignored.
			# It is read from conf. file
			self.N = indata.shape[0]
			self.init_vectors()
			self.qx[:] = indata[:,0]
			self.qy[:] = indata[:,1]
			self.vx[:] = indata[:,2]
			self.vy[:] = indata[:,3]

	def reset_obs(self):
		self.counter = 0
		self.kin = 0.0
		self.pot = 0.0
		self.pre = 0.0

	def add_obs(self, kin, pot, pre):
		self.counter += 1
		self.kin += kin
		self.pot += pot
		self.pre += pre
		self.kinave = self.kin / self.counter
		self.potave = self.pot / self.counter
		self.preave = self.pre / self.counter

	#--------------------------------------------------------------
	# vectors that cointain positions, velocities and accelerations
	# the first index is particle index
	# the second index indicate direction: 0 is x, 1 is y
	#--------------------------------------------------------------
	def init_vectors(self):
		self.qx = np.zeros(self.N, dtype=float) # x position of particles
		self.qy = np.zeros(self.N, dtype=float) # y position of particles
		self.vx = np.zeros(self.N, dtype=float) # x velocity of particles
		self.vy = np.zeros(self.N, dtype=float) # y velocity of particles

	#------------------------------------------------------------
	# create a lattice of N particles
	#------------------------------------------------------------
	def create_lattice(self):
		n=int(math.sqrt(self.N))
		dsx=1.0*self.Lx/n
		dsy=1.0*self.Ly/n
		speed=math.sqrt(self.kIni)
		for i in range(n):
			for j in range(n):
				self.qx[n*i+j]=(0.5+j)*dsx
				self.qy[n*i+j]=(0.5+i)*dsy
				self.vx[n*i+j]=np.random.normal(speed,0.01*speed)
				self.vy[n*i+j]=np.random.normal(speed,0.01*speed)

	def rescale_temp(self, factor):
		self.vx *= factor
		self.vy *= factor

	def rescale_pos(self, factor):
		self.Lx *= factor
		self.Ly *= factor
		self.qx *= factor
		self.qy *= factor

	def reset_momentum(self):
		self.vx -= self.vx.mean()
		self.vy -= self.vy.mean()

	def update_acceleration(self):
		md.md.update_acceleration(self.qx, self.qy, self.N)

	def get_next_position(self):
		self.qx, self.qy, self.vx, self.vy = md.md.get_next_position(self.qx, self.qy, self.vx, self.vy)
