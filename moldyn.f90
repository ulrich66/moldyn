module md
	implicit none

	integer, save :: N, Nm1, Nm2
	real(8), save :: Lx, Lx_half
	real(8), save :: Ly, Ly_half
	real(8), save :: dt, dt2, dt_half, dt2_half
	real(8), save :: kinetic, potential, virial, pressure

	real(8), allocatable, dimension(:), save :: ax, ay

contains

  subroutine init_system(eN, eLx, eLy, edt)
		integer, intent(in)  :: eN
		!f2py intent(in) :: eN
		real(8), intent(in)  :: eLx, eLy, edt
		!f2py intent(in) :: eLx, eLy, edt
		integer :: err
		N   = eN
		Nm1 = N-1
		Nm2 = N-2
		Lx = eLx
		Lx_half = 0.5*Lx
		Ly = eLy
		Ly_half = 0.5*Ly
		dt = edt
		dt_half = 0.5*dt
		dt2 = dt**2
		dt2_half = 0.5*dt2
		allocate(ax(0:Nm1), stat=err)
		allocate(ay(0:Nm1), stat=err)
	end subroutine

	subroutine update_acceleration(qx, qy, eN)
		real(8), dimension(0:eN-1), intent(in) :: qx, qy
		!f2py intent(in) :: qx, qy
		integer, intent(in) :: eN
		integer :: i, j
		real(8) :: dx, dy, r2, rm2, rm6, f_over_r, fx, fy
		real(8) :: tmp_qxj, tmp_qyj
		potential = 0.0
		virial    = 0.0
		do i = 0, Nm1
			ax(i) = 0.0
			ay(i) = 0.0
		end do
		do j = 0, Nm2
			tmp_qxj = qx(j)
			tmp_qyj = qy(j)
			do i = j+1, Nm1
				dx = qx(i) - tmp_qxj
				if (dx >=  Lx_half) dx = dx - Lx
				if (dx < -Lx_half)  dx = dx + Lx
				dy = qy(i) - tmp_qyj
				if (dy >=  Ly_half) dy = dy - Ly
				if (dy < -Ly_half)  dy = dy + Ly
				r2 = dx**2 + dy**2
				rm2 = 1.0/r2
				rm6 = rm2**3
				f_over_r = 24*rm6*(2*rm6-1)*rm2
				fx = f_over_r * dx
				fy = f_over_r * dy
				ax(i) = ax(i) + fx
				ay(i) = ay(i) + fy
				ax(j) = ax(j) - fx
				ay(j) = ay(j) - fy
				potential = potential + 4.0*rm6*(rm6 - 1.0)
				virial = virial + dx * fx + dy * fy
			end do
		end do
	end subroutine

	subroutine get_next_position(qx, qy, vx, vy, eN)
		real(8), dimension(0:eN-1), intent(inout) :: qx, qy, vx, vy
		!f2py intent(in,out) :: qx, qy, vx, vy
		integer, intent(in) :: eN
		integer :: i
		kinetic = 0.0
		do i = 0, Nm1
			qx(i) = qx(i) + vx(i)*dt + ax(i) * dt2_half
			if (qx(i) >= Lx) qx(i) = qx(i) - Lx
			if (qx(i) <   0) qx(i) = qx(i) + Lx
			qy(i) = qy(i) + vy(i)*dt + ay(i)*dt2_half
			if (qy(i) >= Ly) qy(i) = qy(i) - Ly
			if (qy(i) <   0) qy(i) = qy(i) + Ly
	    vx(i) = vx(i) + ax(i) * dt_half
	    vy(i) = vy(i) + ay(i) * dt_half
		end do
	  call update_acceleration(qx, qy, eN)
		do i = 0, Nm1
	    vx(i) = vx(i) + ax(i) * dt_half
	    vy(i) = vy(i) + ay(i) * dt_half
			kinetic = kinetic + vx(i)**2 + vy(i)**2
		end do
		kinetic = 0.5 * kinetic
	end subroutine

	subroutine get_obs(kin, pot, pre)
		real(8), intent(out) :: kin, pot, pre
		!f2py intent(out) :: kin, pot, pre
		kin = kinetic / N
		pot = potential / N
		pre = (kinetic+0.5*virial)/(Lx*Ly)
	end subroutine

end module
