#!/usr/bin/env python3
"""
  md.py - Animated Molecular Dynamics in dim=2

  Usage:
    md.py -h | --help
    md.py --version
    md.py -c <INIfile>

  Options:
    -h --help         Display this help and exit
    --version         Display version and exit
    -c <INIfile>      INI file (without .ini extension)
"""

import os.path
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from time import time
import moldyn as md
import docopt
from configparser import ConfigParser
from particle_system import ParticleSystem

argv = docopt.docopt(__doc__, version="md.py 1.2")
r0 = 2.0**(1.0/6.0)

iniFile = argv['-c'] + ".ini"

config = ConfigParser()
config.sections()
config.read(iniFile)
param = config['DEFAULT']

start = param['start']
N = param.getint('N')
squareBox = param.getboolean('square_box')
linear_expansion = param.getfloat('linear_expansion')

if start == 'lattice':
	if not int(math.sqrt(N))**2 == N:
		sys.exit("\n\n  Number of particles N must be a squared integer \n\n")

Lx = math.sqrt(N) * r0 * linear_expansion
if squareBox:
	Ly = Lx
else:
	Ly = Lx * (0.5*math.sqrt(3.0))

Vol = Lx*Ly
density = N/Vol

tempIni          = param.getfloat('temp_ini')
tempResc         = param.getboolean('temp_resc')
tempRescFactor   = param.getfloat('temp_resc_factor')
tempRescInterval = param.getfloat('temp_resc_interval')
tempRescEnd      = param.getfloat('temp_resc_end')
velRescFactor    = math.sqrt(tempRescFactor)

#------------------------------------------------------------------------------
# at the moment i'm not going to use the following
#densResc         = param.getboolean('dens_resc')
#densRescFactor   = param.getfloat('dens_resc_factor')
#densRescInterval = param.getfloat('dens_resc_interval')
#densRescEnd      = param.getfloat('dens_resc_end')*density
#linRescFactor = math.sqrt(densRescFactor)
#------------------------------------------------------------------------------

dt     = param.getfloat('dt')
Time   = param.getfloat('Time')
tWrite = param.getfloat('write_time')

save_animation = param.getboolean('save_animation')
anifile        = param['anifile'] + '.mp4'

fps       = 24
Ntime     = int(Time/dt)
NcheckMom = int(5/dt)
Nwrite    = int(tWrite/dt)
NrescTemp = int(tempRescInterval/dt)
#NrescDens = int(densRescInterval/dt)
Nframes   = Ntime

#----------------------------------------------------------
# Instantiate our ParticleSystem class
#----------------------------------------------------------
ps = ParticleSystem(N, Lx, Ly, tempIni, dt, start)
ps.reset_momentum()

#----------------------------------------------------------
# Animation setup
#----------------------------------------------------------
markSize = 12
fig = plt.figure(figsize=(8,8))
fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
ax = fig.add_subplot(111, aspect='equal', autoscale_on=False, xlim=(0,ps.Lx), ylim=(0,ps.Ly))
particles, = ax.plot([], [], 'o', ms=markSize, color='#cc2255')
#----------------------------------------------------------
# time_text should go in another subplot
#----------------------------------------------------------
time_text = ax.text(0.1, 0.9, '', transform=ax.transAxes)

#------------------------------------------------------------------------------
def init_anim():
	particles.set_data(ps.qx,ps.qy)
#	time_text.set_text('')
	return particles, time_text
#	return particles

#------------------------------------------------------------------------------
def animate(iframe):
	global density, tempResc
	t = iframe+1

#	start_time = time()
	ps.get_next_position()
#	end_time = time()
#	elaps_time = end_time - start_time
#	print("Verlet time: %f" % (elaps_time))

	kin, pot, pre = md.md.get_obs()
	ps.add_obs(kin, pot, pre)

	if t % Nwrite == 0:
		ideal_pressure = density*ps.kinave
		print("%6.2f %9.6f %9.6f %9.6f %9.6f" \
      	% (dt*t, ps.kinave+ps.potave, ps.kinave, ps.potave, ps.preave/ideal_pressure))
		ps.reset_obs()

	#	if ps.kinave < tempRescEnd:
	#		tempResc = False

	if tempResc and ps.kinave > tempRescEnd and t % NrescTemp == 0:
#		print(tempResc, tempRescEnd, NrescTemp)
		print("Rescaling temperature at time %f of %f" % (t*dt, Time))
		ps.rescale_temp(velRescFactor)

#	if densResc and density > densRescEnd and t % NrescDens == 0:
#		density *= densRescFactor
#		ps.rescale_pos(posRescFactor)

	if t % NcheckMom == 0:
		ps.reset_momentum()

	particles.set_data(ps.qx,ps.qy)
	time_text.set_text('time = %.3e'%(t*dt))
	return particles, time_text
#	return particles
#------------------------------------------------------------------------------

ps.update_acceleration()
interval = 1
ani = animation.FuncAnimation(fig, animate, frames=Nframes, repeat=False,
                              interval=interval, blit=True, init_func=init_anim)
if save_animation:
    ani.save(anifile, fps=fps, extra_args=['-vcodec', 'libx264'])
else:
    plt.show()
